function showFilter() {
    var filter = document.getElementById('filterForm');
    if (filter.style.display === 'none')
        filter.style.display = 'block';
    else
        filter.style.display = 'none';
}

function deleteBook(id) {
    $.ajax({
        url: '/book/'+id,
        success: function () {
            $('#book_'+id).remove();
            $('#editBook_'+id).remove();
        }
    })
}

function editBook(id) {
    var editForm = document.getElementById('editBook_' + id);

    if (editForm.style.display === 'table-row')
        editForm.style.display = 'none';
    else
        editForm.style.display = 'table-row';
}

function getSelected(select) {
    var result = [];
    var options = select && select.options;
    var opt;

    for (var i=0; i<options.length; i++){
        opt = options[i];

        if (opt.selected)
            result.push(opt.value || opt.text);
    }
    return result;
}

function formatDate(dateToFormat) {
    var date = new Date(dateToFormat);
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    day = day < 10 ? "0" + day : day;
    month = month < 10 ? "0" + month : month;
    var formattedDate = day + "/" + month + "/" + year;
    return formattedDate;
}

function updateBook(id) {
    var editForm = document.getElementById('editBook_' + id);
    editForm.style.display = 'none';

    var title = document.getElementById('new_book_title_' + id).value.trim();
    var authors = document.getElementById('new_book_authors_' + id);
    var authorsId = getSelected(authors);
    var description = document.getElementById('new_book_description_' + id).value.trim();
    var date = document.getElementById('new_book_date_' + id).value;
    var picture = document.getElementById('new_book_picture_' + id).files[0];

    // alert(date);

    if (title == ''){
        $('#errorMessage_'+id).text('Write title!');
        return false;
    }
    if (authorsId.length == 0){
        $('#errorMessage_'+id).text('Choose authors!');
        return false;
    }
    if (description == ''){
        $('#errorMessage_'+id).text('Write description!');
        return false;
    }
    if (date == ''){
        $('#errorMessage_'+id).text('Choose date!');
        return false;
    }
    $('#errorMessage_'+id).text('');


    var formData = new FormData();
    formData.append('id', id);
    formData.append('title', title);
    formData.append('authorsId', JSON.stringify(authorsId));
    formData.append('description', description);
    formData.append('picture', picture);
    formData.append('date', date);


    $.ajax({
        url: '/book/'+id+'/editAjax',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function(){

        },
        success: function (data) {
            var data = JSON.parse(data);

            $('#book_title_'+data.id).html(data.title);

            var authors = '';
            for (key in (data.authors)){
                authors += data.authors[key] + '<br>';
            }

            $('#book_authors_'+data.id).html(authors);
            $('#book_description_'+id).html(data.description);
            $('#book_date_'+id).html(data.date);

            if (!(!data.oldPicture && !data.picture)){
                $('#updated_book_picture_'+id).html('');
                $('<img/>')
                    .attr('src', 'uploads/media/default/0001/01/' + data.pictureName)
                    .attr('width', '40px')
                    .attr('height', '50px')
                    .appendTo('#updated_book_picture_'+id);
            }
        }
    })
}