<?php

namespace AppBundle\Form;

use AppBundle\AppBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false
            ])
            ->add('authors', EntityType::class, [
                'class' => 'AppBundle:Author',
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('descriptionByWord', TextType::class, [
                'required' => false
            ])
            ->add('isPicture', ChoiceType::class, [
                'label' => 'With picture',
                'choices' => [
                    '' => 'all',
                    'Yes' =>'yes',
                    'Nope' => 'no'
                ]
            ])
            ->add('dateFrom', DateType::class, [
                'required' => false
            ])
            ->add('dateTo', DateType::class, [
                'required' => false
            ])

            ->add('useFilter', SubmitType::class);

    }
}
