<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Form\Type\Filter\DateType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class BookAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('authors',null, [], null, [
                'choice_label' => 'name'
            ])
            ->add('description')
            ->add(
                'withPicture',
                'doctrine_orm_callback',
                [
                    'callback' => array($this, 'getWithPicture'),
                    'label' => 'With picture'
                ],
                'choice',
                array('choices' => array('Yes' => true, 'Nope' => false))
            )
            ->add('date', 'doctrine_orm_date_range', [], null, []);
    }

    public function getWithPicture($queryBuilder, $alias, $field, $value){

        if ($value['value']){
            dump($value['value']);
            $queryBuilder
                ->andWhere($queryBuilder->expr()->isNotNull($alias.'.picture'));
        }
        else
            $queryBuilder
                ->andWhere($queryBuilder->expr()->isNull($alias.'.picture'));

        return true;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('authors', null, array('associated_property' => 'name'))
            ->add('description')
            ->add('picture', null, ['template' => '@SonataMedia/MediaAdmin/list_image.html.twig'])
            ->add('date')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('description')
            ->add('authors', null, [
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('picture', 'sonata_type_model_list', [], ['link_parameters' =>['context' => 'default']])
            ->add('date');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('authors', null, array('associated_property' => 'name'))
            ->add('description')
            ->add('picture', null, ['template' => '@SonataMedia/MediaAdmin/list_image.html.twig'])
            ->add('date')
        ;
    }

    public function toString($object)
    {
        return $object instanceof Book
            ? $object->getTitle()
            : 'Book'; // shown in the breadcrumb on the create view
    }
}
