<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Form\AuthorType;
use AppBundle\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $authorRepository = $entityManager->getRepository(Author::class);

        return $this->render('@App/Author/index.html.twig', [
            'authors' => $authorRepository->findAll()
        ]);
    }

    public function newAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
                $author = $form->getData();
                $entityManager->persist($author);
                $entityManager->flush();

                return $this->redirectToRoute('author');
        }

        return $this->render('@App/Author/new.html.twig', [
            'author' => $author,
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, Author $author)
    {
        dump($request);
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('author');
        }

        return $this->render('@App/Author/edit.html.twig',[
            'author' => $author,
            'form' => $form->createView()
        ]);
    }

    public function deleteAction(Request $request, Author $author)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($author);
        $entityManager->flush();

        return $this->redirectToRoute('author');
    }


}
