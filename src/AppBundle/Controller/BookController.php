<?php

namespace AppBundle\Controller;

use AppBundle\Application\Sonata\MediaBundle\Entity\Media;
use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use AppBundle\Form\FilterType;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class BookController extends Controller
{
    public function indexAction(Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $authorRepository = $entityManager->getRepository(Author::class);
        $authors = $authorRepository->findAll();
        $bookRepository = $entityManager->getRepository(Book::class);
        $books = $bookRepository->findAll();

        $filterForm = $this->createForm(FilterType::class);
        $filterForm->handleRequest($request);

        // list of books written by more than two authors
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Book::class, 'book');
        $rsm->addFieldResult('book', 'id', 'id');
        $rsm->addFieldResult('book', 'title', 'title');
        $rsm->addJoinedEntityResult(Author::class, 'author', 'book', 'authors');
        $query = $entityManager->createNativeQuery(
            'SELECT book.id, book.title, COUNT(books_authors.author_id) AS count_authors FROM book JOIN books_authors ON book.id = books_authors.book_id GROUP BY book.id HAVING count_authors > 2;',
            $rsm
        );
        $booksWrittenByMoreThanTwoAuthorsV1 = $query->getResult();

        // list of books written by more than two authors
        $query = $bookRepository->createQueryBuilder('book')
            ->join('book.authors', 'authors')
            ->groupBy('book.id')
            ->having('COUNT(authors) > :countAuthors')
            ->setParameter('countAuthors', 2);
        $booksWrittenByMoreThanTwoAuthorsV2 = $query->getQuery()->getResult();
        foreach ($booksWrittenByMoreThanTwoAuthorsV2 as $key => $book){
            $countAuthors[$key] = count($book->getAuthors());
        }


        //filter
        if ($filterForm->isSubmitted()){

            $title = $filterForm['title']->getData();
            $author = $filterForm['authors']->getData();
            $descriptionByWord = $filterForm['descriptionByWord']->getData();
            $isPicture = $filterForm['isPicture']->getData();
            $dateFrom = $filterForm['dateFrom']->getData();
            $dateTo = $filterForm['dateTo']->getData();

            $queryBuilder = $bookRepository->createQueryBuilder('book')
                ->innerJoin('book.authors', 'authors');

            $queryBuilder
                ->andWhere('book.title LIKE :title')
                ->andWhere('book.description LIKE :descriptionByWord')
                ->setParameters(array(
                    'title' => '%'.$title.'%',
                    'descriptionByWord' => '%'.$descriptionByWord.'%'
                ));

            if ($author){
                $queryBuilder
                    ->andWhere('authors.id = :authorId')
                    ->setParameter('authorId', $author->getId());
            }
            if ($dateFrom){
                $queryBuilder
                    ->andWhere('book.date >= :dateFrom')
                    ->setParameter('dateFrom', $dateFrom);
            }
            if ($dateTo){
                $queryBuilder
                    ->andWhere('book.date <= :dateTo')
                    ->setParameter('dateTo', $dateTo);
            }
            if ($isPicture == 'yes')
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->isNotNull('book.picture'));
            if ($isPicture == 'no')
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->isNull('book.picture'));

            $books = $queryBuilder->getQuery()->getResult();
        }

        return $this->render('@App/Book/index.html.twig', [
            'filterForm' => $filterForm->createView(),
            'authors' => $authors,
            'books' => $books
        ]);
    }

    public function newAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $book = $form->getData();
            $picture = $request->files->get('appbundle_book')['picture'];

            if ($picture)
                $book->setPicture('default', 'sonata.media.provider.image', $picture->getClientOriginalName(), $picture);

            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('book');
        }

        return $this->render('@App/Book/new.html.twig', [
            'book' => $book,
            'form' => $form->createView()
        ]);
    }


    public function editAjaxAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $authorRepository = $entityManager->getRepository(Author::class);
        $bookRepository = $entityManager->getRepository(Book::class);
        $pictureRepository = $entityManager->getRepository(Media::class);

        $id = $request->request->get('id');
        $title = $request->request->get('title');
        $authorsId = json_decode($request->request->get('authorsId'));
        $description = $request->request->get('description');
        $picture = $request->files->get('picture');
        dump($request->files->get('picture'));
        $date = date_create_from_format('Y-m-d', $request->request->get('date'));

        $book = $bookRepository->find($id);
        if ($book){

            $book
                ->setTitle($title)
                ->setDescription($description);
            $book->setDate($date);

            $book->getAuthors()->clear();
            foreach ($authorsId as $authorId){
                $author = $authorRepository->find($authorId);
                $book->getAuthors()->add($author);
            }

            $oldPicture = $book->getPicture();
            if ($picture){
                if ($oldPicture)
                    $entityManager->remove($oldPicture);
                $book->setPicture('default', 'sonata.media.provider.image', $picture->getClientOriginalName(), $picture);
            }
            $entityManager->flush();

            $authors = [];
            foreach ($authorsId as $authorId){
                $author = $authorRepository->find($authorId);
                array_push($authors, $author->getName());
            }

            if ($oldPicture)
                $oldPicture = true;
            else
                $oldPicture = false;
            if ($book->getPicture())
                $pictureName = $book->getPicture()->getProviderReference();
            else
                $pictureName = null;

            $data = array (
                'id' => $book->getId(),
                'title' => $book->getTitle(),
                'authors' => $authors,
                'description' => $book->getDescription(),
                'picture' => $book->getPicture(),
                'oldPicture' => $oldPicture,
                'pictureName' => $pictureName,
                'date' => $book->getDate()->format('d/m/Y')
            );
            dump($book->getDate()->format('d/m/Y'));

            echo json_encode($data);

            return new Response('', 200);
        }

        return new Response('', 500);
    }

    public function deleteAction(Request $request, Book $book)
    {
        $entityManager = $this->getDoctrine()->getManager();


        $picture = $book->getPicture();
        if ($picture){
            $path = $this->getParameter('pictures_directory').'/'.$picture;

            $fileSystem = new Filesystem();
            $fileSystem->remove(array($path));
        }


        $entityManager->remove($book);
        $entityManager->flush();

        if($request->isXmlHttpRequest())
            return new Response('', 200);



        return $this->redirectToRoute('book');
    }


}
